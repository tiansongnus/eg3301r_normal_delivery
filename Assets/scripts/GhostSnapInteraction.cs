using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostSnapInteraction : MonoBehaviour
{
    public GameObject ToActivate;
    public GameObject ToDeactivate;
    public string tagToCheck;

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == tagToCheck)
        {
            other.gameObject.SetActive(false);
            ToDeactivate.SetActive(false);
            ToActivate.SetActive(true);
        }
    }
    
}
