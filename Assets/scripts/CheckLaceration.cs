using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckLaceration : MonoBehaviour
{
    public GameObject[] lacerations;
    private int lacerationCount;
    public placentatray placentaTray;
    // Start is called before the first frame update
    void Start()
    {
        lacerationCount = lacerations.Length;
    }

    public void lacerationSpotted()
    {
        lacerationCount--;
        if (lacerationCount <= 0)
        {
            lacerationDone();
        }
    }

    private void lacerationDone()
    {
        placentaTray.CallClean();
    }
}
