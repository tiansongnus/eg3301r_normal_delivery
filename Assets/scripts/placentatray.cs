using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class placentatray : MonoBehaviour
{
    public GameObject placentaintray;
    public GameObject cleaninstruct;
    public GameObject cleanprog;
    public GameObject cottray;
    public GameObject rightlab;
    public GameObject placentaInstruct;
    public GameObject ownMesh;
    public GameObject laceration;


    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "tray")
        {
            other.gameObject.SetActive(false);
            placentaInstruct.SetActive(false);
            ownMesh.SetActive(false);
            placentaintray.SetActive(true);
            StartCoroutine(WaitForNext());
        }
    }
    
    private IEnumerator WaitForNext()
    {
        yield return new WaitForSeconds(1);
        cleaninstruct.SetActive(true);
        StartCoroutine(PartLabia());
    }

    private IEnumerator PartLabia()
    {
        yield return new WaitForSeconds(4);
        cleaninstruct.GetComponent<PartLabia>().StartPartLabia();
        StartCoroutine(StartLaceration());
    }

    private IEnumerator StartLaceration()
    {
        yield return new WaitForSeconds(4);
        cleaninstruct.GetComponent<PartLabia>().FinishPartLabia();
        cleaninstruct.SetActive(false);
        laceration.SetActive(true);
    }


    public void CallClean()
    {
        StartCoroutine(WaitForNext2());
        laceration.SetActive(false);
    }

    private IEnumerator WaitForNext2()
    {
        yield return new WaitForSeconds(1);
        cottray.SetActive(true);
        cleanprog.SetActive(true);
        rightlab.SetActive(true);
    }

    //set active wool stuff, and nstructions 
}
;