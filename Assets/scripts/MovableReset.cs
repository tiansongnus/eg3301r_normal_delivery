using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableReset : MonoBehaviour
{
    private Vector3 initialPos;
    private Quaternion initialRot;
    void Awake()
    {
        initialPos = transform.position;
        initialRot = transform.rotation;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ResetPos()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        transform.position = initialPos;
        transform.rotation = initialRot;
    }
}
