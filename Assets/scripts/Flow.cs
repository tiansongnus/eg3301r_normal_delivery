using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Flow : MonoBehaviour
{
    public bool checklist = false;
    public bool question = false;
    public bool lengthen = false;
    public bool bloodboo = false;
    public bool clean = false;


    public bubut check1;
    public animbar bar;
    public GameObject test;

    public GameObject login;
    public GameObject checklistGO;
    public GameObject userInputField;
    public GameObject mother;
    public GameObject bloodparent;
    public GameObject rubAbdomen;
    public GameObject lengthening;
    public GameObject glob;
    public GameObject signsSummary;
    public GameObject bedPlacentaTray;
    public GameObject pull;
    public GameObject rot;
    public GameObject rotparent;
    public GameObject oldTray;
    public GameObject placentamove;
    public GameObject placentaTrayFinalLocation;
    public GameObject oldp;
    public GameObject traypop;
    public GameObject cleaningbar;
    public GameObject rubAbdomenSecond;
    public GameObject summary;

    public GameObject CCTOverall;
    public GameObject cctdom;
    public GameObject cctnondom;

    public QuestionManager questionManager;
    public FirebaseManager firebaseManager;

    public Animator motheranim;

    //public Animation anim;
    private bool traceComplete = false;
    private bool bedPlacentaActivated = false;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!checklist && checklistGO.activeInHierarchy == true)
        {
            check1.lastcheck();
            //if checklist not true, continues to run lastcheck func
            //once all 6 check, checklist = true, open questions
        }
        if (rubAbdomen.activeInHierarchy == true && (rubAbdomen.GetComponent<animbar>().IsDone() == true))
        {
            Debug.Log("rub finished");
            rubAbdomen.GetComponent<animbar>().finishRubAbdomen();
            questionManager.callquestion();
        }
        if (motheranim.GetCurrentAnimatorStateInfo(0).IsName("Lengthen") &&
            motheranim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1.0f)
        {
            Debug.Log("ok");
            glob.GetComponent<animbar>().globulary();

        }
        if (motheranim.GetCurrentAnimatorStateInfo(0).IsName("Globular") &&
    motheranim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1.0f)
        {
            Debug.Log("ok");
            glob.GetComponent<animbar>().bloody();
            //motheranim.Play("New State");
        }
        if (bloodboo && motheranim.GetCurrentAnimatorStateInfo(0).IsName("default") &&
    motheranim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.5f)
        {
            showSignsSummary();
        }
        if (bedPlacentaTray.activeInHierarchy == true && bedPlacentaActivated == false)
        {
            cct();
        }
        if (CCTOverall.GetComponent<CctOverall>().IsCCTComplete())
        {
            oldp.SetActive(false);
            rotparent.SetActive(false);
            oldTray.SetActive(false);
            traypop.SetActive(true);
            CCTOverall.SetActive(false);
            placentaTrayFinalLocation.SetActive(true);
            placentamove.SetActive(true);
        }
    //    if (motheranim.GetCurrentAnimatorStateInfo(0).IsName("umbpull") &&
    //motheranim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1.0f)
        if (CCTOverall.GetComponent<CctOverall>().IsCCTPullComplete())
        {
            Debug.Log("ok");
            rot.GetComponent<animbar>().rotateout();
        }
        if (clean && (cleaningbar.GetComponent<animbar>().number == 2))
        {
            cleaningbar.GetComponent<animbar>().cleaning();
            rubAbdomenSecond.transform.parent.gameObject.SetActive(true);
            rubAbdomenSecond.GetComponent<animbar>().RubAbdomen();
        }
        if (rubAbdomenSecond.activeInHierarchy == true && (rubAbdomenSecond.GetComponent<animbar>().IsDone() == true))
        {
            Debug.Log("rub finished");
            rubAbdomenSecond.GetComponent<animbar>().finishRubAbdomen();
            questionManager.callquestion();
        }


    }

    public void TryRegisterUser()
    {
        login.SetActive(false);
        checklistGO.SetActive(true);
        //string text = userInputField.GetComponent<TMP_InputField>().text;
        //if (firebaseManager.RegisterPlayer(text))
        //{
        //    login.SetActive(false);
        //    checklistGO.SetActive(true);
        //} else
        //{
        //    userInputField.GetComponent<TMP_InputField>().text = "Invalid input";
        //}
    }
    
    public void StartLengthen()
    {
        lengthening.transform.parent.parent.gameObject.SetActive(true);
        lengthening.GetComponent<animbar>().lengthening();
    }

    public void showSignsSummary()
    {
        bloodparent.SetActive(false);
        bloodboo = false;
        //cctnondom.SetActive(false);
        signsSummary.SetActive(true);
        signsSummary.GetComponent<Animator>().SetTrigger("pop");
    }

    public void cct()
    {
        CCTOverall.SetActive(true);
        bedPlacentaActivated = true;
        StartCoroutine(WaitForNext());
    }

    private IEnumerator WaitForNext()
    {
        yield return new WaitForSeconds(1f);
        Debug.Log("yeet");
        cctdom.SetActive(true);
        StartCoroutine(WaitForNext2());
    }

    private IEnumerator WaitForNext2()
    {
        yield return new WaitForSeconds(1f);
        Debug.Log("nay");
        cctnondom.SetActive(true);
        StartCoroutine(WaitForNext3());
    }

    private IEnumerator WaitForNext3()
    {
        yield return new WaitForSeconds(4f);
        pull.GetComponent<animbar>().pullcord();
    }

    public void ToggleTraceComplete()
    {
        traceComplete = !traceComplete;
    }


}

