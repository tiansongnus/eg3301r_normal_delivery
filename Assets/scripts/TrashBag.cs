using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashBag : MonoBehaviour
{
    private GameObject movable;
    private string movableTag;
    public TraceColliders traceCollider;
    public QuestionManager questionManager;
    public GameObject disposePlacentaInstruct;

    public void UpdateMovableData(GameObject movable, string movableTag)
    {
        if (movable != null)
        {
            this.movable = movable;
        }
        else if (!string.IsNullOrEmpty(movableTag))
        {
            this.movableTag = movableTag;
        }
        Debug.Log(this.movableTag);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject == movable || other.gameObject.CompareTag(this.movableTag))
        {
            Destroy(other.gameObject);
            traceCollider.ProgressCollider();
        } else if (other.gameObject.tag == "placenta")
        {
            Destroy(other.gameObject);
            disposePlacentaInstruct.SetActive(false);
            questionManager.callquestion();
        }
    }
}
