using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour
{
    void OnCollisionStay(Collision collision)
    {
        MovableReset objectReset = collision.gameObject.GetComponent<MovableReset>();
        if (objectReset != null)
        {
            objectReset.ResetPos();
        }
    }
}
