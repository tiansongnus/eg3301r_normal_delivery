using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CctOverall : MonoBehaviour
{

    public GameObject CCTRHand;
    public GameObject CCTLHand;
    public GameObject CCTHolder;
    public GameObject forceps;
    public GameObject oldPlacenta;
    public GameObject animatedPlacenta;
    public GameObject nonDomInstruct;
    public GameObject DomInstruct;
    private Animator forcepAnimator;
    private Animator placentaAnimator;
    private Animator CCTRAnimator;
    private Animator CCTLAnimator;

    // Start is called before the first frame update
    void Start()
    {
        

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void prepCCT()
    {
        nonDomInstruct.SetActive(false);
        DomInstruct.SetActive(false);
        CCTHolder.SetActive(true);

    }

    public void StartCCT()
    {
        CCTLAnimator = CCTLHand.GetComponent<Animator>();
        CCTRAnimator = CCTRHand.GetComponent<Animator>();
        forcepAnimator = forceps.GetComponent<Animator>();
        placentaAnimator = animatedPlacenta.GetComponent<Animator>();
        oldPlacenta.SetActive(false);
        CCTLAnimator.Play("CCT");
        CCTRAnimator.Play("CCT");
        forcepAnimator.Play("CCT");
        placentaAnimator.Play("CCT");
    }

    public bool IsCCTPullComplete()
    {
        return animatedPlacenta.activeInHierarchy && placentaAnimator.GetCurrentAnimatorStateInfo(0).IsName("CCT") &&
    placentaAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.6f;
    }

    public bool IsCCTComplete()
    {
        return animatedPlacenta.activeInHierarchy && placentaAnimator.GetCurrentAnimatorStateInfo(0).IsName("CCT") &&
    placentaAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1.0f;
    }

    public void FinishCCT()
    {
        
    }
}
