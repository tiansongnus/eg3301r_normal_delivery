using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartLabia : MonoBehaviour
{
    public GameObject PLRHand;
    public GameObject PLLHand;
    public Animator LabiaAnimator;
    private Animator PLRAnimator;
    private Animator PLLAnimator;

    // Start is called before the first frame update
    void Start()
    {
        PLLAnimator = PLLHand.GetComponent<Animator>();
        PLRAnimator = PLRHand.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void StartPartLabia()
    {
        LabiaAnimator.enabled = true;
        PLLAnimator.Play("PartLabia");
        PLRAnimator.Play("PartLabia");
        LabiaAnimator.Play("PartLabia");
    }

    public void FinishPartLabia()
    {
        LabiaAnimator.Play("Rest");
        PLRHand.SetActive(false);
        PLLHand.SetActive(false);
    }
}
