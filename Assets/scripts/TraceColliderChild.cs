using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraceColliderChild : MonoBehaviour
{
    private GameObject movable;
    private string movableTag;
    public TraceColliders container;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateMovableData(GameObject movable, string movableTag)
    {
        if (movable != null)
        {
            this.movable = movable;
        } else if (!string.IsNullOrEmpty(movableTag))
        {
            this.movableTag = movableTag;
        }
        Debug.Log(this.movableTag);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject == movable || other.gameObject.CompareTag(this.movableTag))
        {
            container.ProgressCollider();
        }
    }
}
