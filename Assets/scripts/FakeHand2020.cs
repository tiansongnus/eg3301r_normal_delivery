﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeHand2020 : MonoBehaviour

{
    
    //public GameObject bed;
    public GameObject righthand;
    public GameObject lefthand;
    //public GameObject lefthandref;
    public GameObject leftglove;
    public GameObject rightglove;
    public GameObject birthCanvas;
    public GameObject leftmaterial;
    public GameObject rightmaterial;
    public GameObject message;
    public GameObject servercommunicator;
    public GameObject mother;
    public GameObject[] warnDelivery;



    public Material green;
    public Material red;
    public float lhanddist;
    public float rhanddist;
    private int i=0;
    public bool handIn = false;
    private Color32 TransRed = new Color32(255, 0, 0, 100);
    private Color32 TransGreen = new Color32(0, 255, 0, 100);
    public float RotL;
    public float RotR;
    // Start is called before the first frame update
    void Start()
    {
        //Physics.IgnoreCollision(bed.GetComponent<Collider>(), GetComponent<Collider>());
    }

    // Update is called once per frame
    void Update()
    {
        i++;
        if (i > 30)
        {
            lhanddist = (leftglove.transform.position - lefthand.transform.position).magnitude;
            rhanddist = (rightglove.transform.position - righthand.transform.position).magnitude;
            RotL = Quaternion.Angle(leftglove.transform.rotation, lefthand.transform.rotation);
            RotR = Quaternion.Angle(rightglove.transform.rotation,righthand.transform.rotation);

            float LDistscore = Mathf.InverseLerp((float)0.2, 0, lhanddist);
            float RDistscore = Mathf.InverseLerp((float)0.2, 0, rhanddist);
            float LRotscore = Mathf.InverseLerp(45 ,5 , RotL);
            float RRotscore = Mathf.InverseLerp(45 ,5 , RotR);//Debug.Log("Rot:" + RotR);
            float Lscore = (2 * LDistscore * LRotscore) / ((float)0.00001 + LDistscore + LRotscore);
            float Rscore = (2 * RDistscore * RRotscore) / ((float)0.00001 + RDistscore + RRotscore);
            //mother.GetComponent<isOutHandler>().Lscore = Lscore;
            //mother.GetComponent<isOutHandler>().Rscore = Rscore;
            //mother.GetComponent<Try_anim2>().Lscore = Lscore;
            //mother.GetComponent<Try_anim2>().Lscore = Lscore;





            leftmaterial.GetComponent<SkinnedMeshRenderer>().material.color = Color32.Lerp(TransRed,TransGreen, Lscore);
            rightmaterial.GetComponent<SkinnedMeshRenderer>().material.color = Color32.Lerp(TransRed,TransGreen, Rscore);
            //leftglove.GetComponent<SkinnedMeshRenderer>().material = green;
            //birthCanvas.GetComponent<TMPro.TextMeshProUGUI>().SetText("Rotation" + RotR);
            /* Debug.Log("2*LDistscore*LRotscore" + (2 * LDistscore * LRotscore));
             Debug.Log("LDistscore + LRotscore  " + (LDistscore + LRotscore));
             Debug.Log("Lscore  " + Lscore);
             Debug.Log("Lscore  " + Lscore);*/
            //servercommunicator.GetComponent<TransferData>().LogScore(Lscore, Rscore);
            //update message

            message.GetComponent<TMPro.TextMeshProUGUI>().SetText("Hand is following\n" + "L:" + LDistscore.ToString("P") + " R:" + RDistscore.ToString("P") + "\n Rot L:" + LRotscore.ToString("P") + "\n Rot R:" + RRotscore.ToString("P")+ "\nL:"+ Lscore.ToString("P") + "  R:" + Rscore.ToString("P"));

            if (handIn) //need to use invokerepeating?
            {
                warnDelivery[0].SetActive(true);
                warnDelivery[1].SetActive(false);
            }
            else if (!handIn)
            {
                warnDelivery[0].SetActive(false);
                warnDelivery[1].SetActive(true);
            }

            i = 0; 
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        //rightglove.GetComponent<SkinnedMeshRenderer>().material = green;
        //leftglove.GetComponent<SkinnedMeshRenderer>().material = green;
        handIn = true;
        //Debug.Log(collision.gameObject.name);
    }

    private void OnCollisionStay(Collision collision)
    {
       // rightglove.GetComponent<SkinnedMeshRenderer>().material = green;
        //leftglove.GetComponent<SkinnedMeshRenderer>().material = green;
        handIn = true;
        //Debug.Log(collision.gameObject.name);

    }

    private void OnCollisionExit(Collision collision)
    {
        //rightglove.GetComponent<SkinnedMeshRenderer>().material = red;
        //leftglove.GetComponent<SkinnedMeshRenderer>().material = red;
        handIn = false;
    }
}
