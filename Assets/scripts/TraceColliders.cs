using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraceColliders : MonoBehaviour
{

    public cleaning clean;
    public GameObject movable;
    public string movableTag;
    public TraceColliderChild[] colliderList;
    private int colliderCount = 0;
    public TrashBag trashBag;

    private void OnEnable()
    {
        colliderCount = 0;
        while (colliderCount < colliderList.Length)
        {
            if (colliderCount > 0)
            {
                colliderList[colliderCount].gameObject.SetActive(false);
            } else
            {
                colliderList[colliderCount].gameObject.SetActive(true);
            }
            colliderList[colliderCount++].UpdateMovableData(movable, movableTag);
        }
        colliderCount = 0;
        trashBag.UpdateMovableData(movable, movableTag);
    }

    public void ProgressCollider()
    {
        colliderList[colliderCount++].gameObject.SetActive(false);
        if (colliderCount < colliderList.Length)
        {
            colliderList[colliderCount].gameObject.SetActive(true);
        } else
        {
            CompleteTrace();
        }
    }

    public void CompleteTrace()
    {
        clean.ProgressSwab();
    }
}
