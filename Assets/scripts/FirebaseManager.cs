using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Proyecto26;
using UnityEditor;

//NEED login screen (to get the user ID; 0123)

public class FirebaseManager : MonoBehaviour
{
    private const string databaseUrl = "https://simulationdata-43b0a-default-rtdb.asia-southeast1.firebasedatabase.app/";
    private const string projId = "simulationdata-43b0a";
    private string databaseUrlAlt = $"https://{projId}.firebaseio.com/";

    //Identifying playerID
    public static string playerName;

    //To collect user's answer
    public StepData[] stepDataArray = new StepData[8]; //Need to adjust lenght to final list of questions
    private int questionCounter = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    public bool RegisterPlayer(string input)
    {
        if (input != "")
        {
            playerName = input + "_1";
            return true;
        } else
        {
            return false;
        }
    }

    //Data collection --> to store string value from the question into stepArray
    public void DataCollection(string ans)
    {
        Debug.Log("Collected");
        stepDataArray[questionCounter] = new StepData();
        stepDataArray[questionCounter].ans = ans;
        questionCounter += 1;
    }

    //When open summary; run OnSubmit
    public void OnSubmit()
    {
        Debug.Log("Submitted");
        PostToDatabaseChild("products/" + playerName);
    }

    private void PostToDatabaseChild(string inner)
    {
        int counter = 0;
        while (counter < questionCounter)
        {
            RestClient.Put($"{databaseUrl}{inner}/step{counter}.json", stepDataArray[counter]);
            counter += 1;
        }
    }

    //private void PostToDatabase()
    //{

    //    RestClient.Put(databaseUrl, user);
    //    Debug.Log(JsonUtility.ToJson(user));
    //    RestClient.Post("https://firestore.googleapis.com/v1/projects/eg4301-aed8/databases/(default)/documents/players?key=AIzaSyBAO6tHCifJ6hQdT3SshO3suAS2vyNk5LY", new string[] { "abc" });
    //    //RestClient.Post("https://firestore.googleapis.com/v1/projects/eg4301-aed8/databases/(default)/documents/players?key=AIzaSyBAO6tHCifJ6hQdT3SshO3suAS2vyNk5LY", new Dictionary<string, string> { { "correctness", "true" }, { "selectedOption", "A" } });

    //}

    //public void OnGetScore()
    //{
    //    RetrieveFromDatabase();
    //}

    //private void UpdateScore()
    //{
    //    scoreText.text = "Score: " + user.userScore;

    //}

  

    

    //private void RetrieveFromDatabase()
    //{
    //    RestClient.Get<SimulationCount>("https://eg4301normaldelivery-ca0db.asia-southeast1.firebasedatabase.app/students/simulation/" + nameText.text + "/simulationCount.json").Then(response =>
    //    {
    //        simCount = response;
    //        Debug.Log(simCount.simulationCount++);
    //    });
    //}
}

public class StepData
{
    public string ans;
    //public string timestamp;
    //public float actionScore = -1.0f;
}

