using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class syringewrong : MonoBehaviour
{
    public int id;


    public Image leg;
    public Image elbow;
    public Image arm;
    public Image ankle;
    private Vector3 startPos;

    public AudioSource source;
    public AudioClip correct;
    public AudioClip wrong;
    public Animator syringe;
    public GameObject syringeanim;
    public GameObject syringereal;
    public GameObject oxytocin;
    public GameObject colliderpopup;
    public Flow flow;
    public GameObject lparent;

    void OnTriggerStay(Collider other)
    {
        Debug.Log(this.id);
        if (this.id == 2)
        {
            Debug.Log("uhhuh");
            if (other.gameObject.tag == "syringe")
            {
                other.transform.position = startPos;
                elbow.color = new Color32(221, 125, 133, 255);
                source.PlayOneShot(wrong);
            }
        }
        else if (this.id == 3)
        {
            if (other.gameObject.tag == "syringe")
            {
                other.transform.position = startPos;
                arm.color = new Color32(221, 125, 133, 255);
                source.PlayOneShot(wrong);
            }
        }
        else if (this.id == 4)
        {
            if (other.gameObject.tag == "syringe")
            {
                other.transform.position = startPos;
                ankle.color = new Color32(221, 125, 133, 255);
                source.PlayOneShot(wrong);
            }
        }
        else if (this.id == 1) //correct
        {
            if (other.gameObject.tag == "syringe")
            {
                source.PlayOneShot(correct);
                Destroy(syringereal);
                leg.color = new Color32(157, 235, 157, 255);
                syringeanim.SetActive(true);
                syringe.GetComponent<Animator>().Play("injection");
                StartCoroutine(WaitForNext());
            }
            //Debug.Log(colliderpopup.GetComponent<colliderpopup>().activee);
            //Debug.Log(flow.checklist);
        }
    }

    private IEnumerator WaitForNext()
    {
        yield return new WaitForSeconds(3);
        syringeanim.SetActive(false);
        oxytocin.SetActive(false);
        //colliderpopup.GetComponent<animbar>().lengthening();
        //GameObject.Find("Lengthen/LProgressbar").lengthening();
        colliderpopup.SetActive(true);
        lparent.GetComponent<animbar>().RubAbdomen();
    }

        void Start()
    {
        startPos = syringereal.transform.position;
    }
}
