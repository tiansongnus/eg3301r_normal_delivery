﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderBehaviour : MonoBehaviour
{
    public Slider slider;
    public GameObject canvas;

    public void Update()
    {
        if (slider.value > 0.50) 
        {
            canvas.SetActive(true);
        }

        if (slider.value < 0.1)
        {
            canvas.SetActive(false);
        }
    }
}