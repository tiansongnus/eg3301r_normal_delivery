﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class animbar : MonoBehaviour
{
    public Image progressBar;
    public GameObject canvas;
    public GameObject globe;
    public GameObject blood;
    public GameObject particle;
    public GameObject placentapull;
    public GameObject placentarot;
    public GameObject cottray;
    public GameObject coll;
    public GameObject cleanparent;
    public Image popup;

    public Flow flow;
    public Animator mother;
    public CctOverall cctOverall;

    public int id;

    public float progress = 1;
    public float number = 0;
    public float Maxx = 100;
    float lerpSpeed;
    // Start is called before the first frame update
    void Start()
    {
        //progress = 1;
        //Maxx = 100;
    }

    // Update is called once per frame
    void Update()
    {
        ProgressFiller();
        lerpSpeed = 1f * Time.deltaTime;
        if (progress/Maxx == 1)
        {
            Debug.Log(this);
        }
    }

    void ProgressFiller()
    {
        progressBar.fillAmount = Mathf.Lerp(progressBar.fillAmount, progress, lerpSpeed);
    }

    public void ProgDone(float Points)
    {
        if (progress < Maxx)
            progress += Points/Maxx;
        number += 1;
    }

    public bool IsDone()
    {
        Debug.Log("Progress: " + progressBar.fillAmount);
        return System.Math.Round(progressBar.fillAmount,2) == 1;
    }

    private void done ()
    {
        if (progress == Maxx)
        {
            popup.color = new Color32(255, 0, 0, 0);
        }
    }

    public void RubAbdomen()
    {
        canvas.GetComponent<colliderpopup>().changee();
    }

    public void finishRubAbdomen()
    {
        canvas.SetActive(false);
    }

    public void lengthening()
    {
        canvas.SetActive(true);
        canvas.GetComponent<colliderpopup>().changee();
        mother.GetComponent<Animator>().Play("Lengthen");
    }

    public void globulary()
    {
        canvas.SetActive(false);
        globe.SetActive(true);
        globe.GetComponent<colliderpopup>().changee();
        mother.GetComponent<Animator>().Play("Globular");
    }
    
    public void bloody()
    {
        globe.SetActive(false);
        blood.SetActive(true);
        blood.GetComponent<colliderpopup>().changee();
        particle.SetActive(true);
        mother.GetComponent<Animator>().Play("default");
        flow.bloodboo = true;
    }

    public void pullcord()
    {
        placentapull.SetActive(true);
        placentapull.GetComponent<colliderpopup>().changee();
        cctOverall.prepCCT();
        cctOverall.StartCCT();
        //mother.GetComponent<Animator>().Play("umbpull");
    }

    public void rotateout()
    {
        placentarot.SetActive(true);
        placentapull.SetActive(false);
        placentarot.GetComponent<colliderpopup>().changee();
        //mother.GetComponent<Animator>().Play("umbrotate");
    }

    public void cleaning()
    {
        cleanparent.SetActive(false);
        flow.clean = false;
        cottray.SetActive(false);
        coll.SetActive(false);
    }
}

