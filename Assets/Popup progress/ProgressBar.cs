﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    public Image progressBar;
    public GameObject canvas;
    public Image popup;

    public float progress = 1;
    float Maxx = 100;
    float lerpSpeed;
    // Start is called before the first frame update
    void Start()
    {
        progress = 0;
        Maxx = 100;
    }

    // Update is called once per frame
    
    void Update()
    {
        ProgressFiller();
        lerpSpeed = 3f * Time.deltaTime;
        if (progress == Maxx)
        {
        //    done();
            canvas.SetActive(false);
        }
    }

    void ProgressFiller()
    {
        progressBar.fillAmount = Mathf.Lerp(progressBar.fillAmount, progress / Maxx, lerpSpeed);
    }

    public void ProgDone(float Points)
    {
        if (progress < Maxx)
            progress += Points;
    }

    private void done ()
    {
        if (progress == Maxx)
        {
            popup.color = new Color32(255, 0, 0, 0);
        }
    }

}

