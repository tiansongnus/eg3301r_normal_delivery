﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionManager : MonoBehaviour
{
    public List<QuestionsAnswers> QnA;
    public GameObject canvas;
    public GameObject count;

    public Text QuestionTxt;
    public Flow flow;
    public FirebaseManager firebaseManager;
    public void Start()
    {
        //generateQuestion();
    }

    public void callquestion()
    {
        StartCoroutine(GenerateQuestion());
    }

    private IEnumerator GenerateQuestion()
    {
        yield return new WaitForSeconds(0.3f);
        if (QnA.Count > count.GetComponent<Counter>().x)
        {
            QuestionsAnswers currQns = QnA[count.GetComponent<Counter>().x];
            currQns.questionObject.gameObject.SetActive(true);
            currQns.questionObject.GetComponent<Question>().QuestionTxt.text = currQns.Question;
            SetAnswers();
            count.GetComponent<Counter>().x++;
            Debug.Log(count.GetComponent<Counter>().x);
        }
        else
        {
            Debug.Log("Out of Questions");

        }
    }

    void SetAnswers()
    {
        QuestionsAnswers currQns = QnA[count.GetComponent<Counter>().x];
        GameObject[] options = currQns.questionObject.GetComponent<Question>().options;
        for (int i = 0; i < options.Length; i++)
        {
            AnswerScript currAnswerScript = options[i].GetComponent<AnswerScript>();
            currAnswerScript.AssignFirebaseManager(firebaseManager);
            currAnswerScript.isCorrect = false;
            string currAnswer = currQns.Answers[i];
            options[i].SetActive(!(currAnswer == ""));
            options[i].transform.GetChild(0).GetComponent<Text>().text = currAnswer;
            options[i].GetComponent<Image>().color = options[i].GetComponent<AnswerScript>().startColor;

            if (QnA[count.GetComponent<Counter>().x].CorrectAnswer == i + 1)
            {
                currAnswerScript.isCorrect = true;
            }
        }

    }
}

