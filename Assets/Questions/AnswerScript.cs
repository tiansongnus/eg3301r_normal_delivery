﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswerScript : MonoBehaviour
{
    public bool isCorrect = false;
    public GameObject canvas;
    public Color startColor;
    public AudioSource source;
    public AudioClip correct;
    public AudioClip wrong;
    public GameObject pop;
    public Text myAns;
    private FirebaseManager firebaseManager;
    private void Start()
    {
        startColor = GetComponent<Image>().color;
    }

    public void Answer()
    {
        pop.GetComponent<Question>().DisableOptions();

        firebaseManager.DataCollection(myAns.text);

        if (isCorrect)
        {
            GetComponent<Image>().color = new Color32 (157,235,157,255);
            source.PlayOneShot(correct);
            Debug.Log("Correct Answer");
            StartCoroutine(WaitForNext());
        }
        else
        {
            GetComponent<Image>().color = new Color32(221,125,133,255);
            source.PlayOneShot(wrong);
            Debug.Log("Wrong Answer");
            StartCoroutine(WaitForNext());
        }
    }

    private IEnumerator WaitForNext()
    {
        yield return new WaitForSeconds(2);
        canvas.SetActive(false);
        pop.GetComponent<Popanimation>().PopUp();
        
    }

    public void AssignFirebaseManager(FirebaseManager firebaseManager)
    {
        this.firebaseManager = firebaseManager;
    }
}
