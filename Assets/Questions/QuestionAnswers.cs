﻿[System.Serializable]

public class QuestionsAnswers
{
    public UnityEngine.GameObject questionObject;
    public string Question;
    public string[] Answers;
    public int CorrectAnswer;
}
