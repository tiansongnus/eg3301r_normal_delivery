using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerQuestion : MonoBehaviour
{
    private QuestionManager questionManager;
    private bool hasTrigger = false;
    // Start is called before the first frame update
    void Start()
    {
        questionManager = FindObjectOfType<QuestionManager>();
    }

    public void StartQuestion()
    {
        if (!hasTrigger)
        {
            questionManager.callquestion();
            hasTrigger = true;
        }
    }
}
