using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Question : MonoBehaviour
{
    public GameObject[] options;
    public Text QuestionTxt;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DisableOptions()
    {
        foreach (GameObject option in options)
        {
            option.GetComponent<Button>().interactable = false;
        }
    }
}
